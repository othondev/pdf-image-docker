#PDF Image Docker
The project to create a microservice to create thumbnails;

## Getting Started

Basic Mode
```bash
docker run --rm -p3000:3000 othondev/pdf-image-docker
```
### Prerequisites
* Docker installed

## License
MIT

## TODO List
* Tests
* Put on docker registry
