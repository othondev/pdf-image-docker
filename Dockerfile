FROM node:10-alpine

RUN apk add --no-cache imagemagick ghostscript poppler-utils

COPY policy.xml /var/www/policy.xml

WORKDIR /var/www
COPY package-lock.json package.json ./

ARG NODE_ENV
ENV NODE_ENV "$NODE_ENV"
RUN npm install \
    && npm cache clean --force

COPY server.js ./

ENTRYPOINT [ "npm", "start" ]

EXPOSE 3000