const express = require('express')
const app = express();
const fs = require('fs');
const crypto = require('crypto');
const { PDFImage } = require("pdf-image");
const axios = require('axios');
const { promisify } = require('util');

const path = '/tmp'
const hash = (url) => crypto.createHmac('sha256', 'The best lib forever =)').update(url).digest('hex')
const filePath = url => `${path}/${hash(url)}`

const downloadFile = async (url) => {
    const path = filePath(url)
    const response = await axios({method: 'GET', url, responseType: 'stream' })

    response.data.pipe(fs.createWriteStream(path))
    return new Promise((resolve, reject) => {
        response.data.on('end', () => {
            console.info('Download is done',{file: path})
            resolve(path)
        })

        response.data.on('error', (err) => {
            console.error('Download erro',{err})
            reject()
        })
      })
}
const returnFilePath = (url) => {
    const isExist = fs.existsSync(filePath(url))
    if(isExist){
        console.info('File cached was used')
        return Promise.resolve(filePath(url))
    } else {
        console.info('Download is needed')
        return downloadFile(url)
    }
}

const returnThumbnail = async (url, pageNumber, res) => {
  const pdfPath = await returnFilePath(url)
  const pdfImage = new PDFImage(pdfPath)

  return pdfImage.convertPage(pageNumber)
    .then(imagePath => res.sendFile(imagePath))
    .catch(err => res.status(500).send(err))
}

app.get(/(.*\.pdf)\/([0-9]+).png$/i, (req, res) => {
  const url = req.params[0].substring(1);
  const pageNumber = req.params[1]
  returnThumbnail(url, pageNumber, res)
})

app.post('/', async (req, res) => {
  const { url, pageNumber } = req.body
  returnThumbnail(url, pageNumber, res)
})

app.listen(3000);
